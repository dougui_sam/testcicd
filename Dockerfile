FROM openjdk:latest
COPY hangman/target/hangman.jar hangman.jar
CMD java -jar hangman.jar